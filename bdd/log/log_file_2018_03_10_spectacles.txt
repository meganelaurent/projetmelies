Le : 10/03/2018
Base de donn�es : spectacles.sqlite
Requ�tes valides effectu�es et r�sultats correspondant


SELECT nom, prenom, resa.nb_places, date_reservation, libelle, heure, date, prix FROM client AS c, reservation AS resa, place AS p, representation AS r, spectacle AS s WHERE resa.id_client = c.id AND resa.id_place = p.id AND p.id_representation = r.id AND r.id_spectacle = s.id ORDER BY c.id
nom           prenom           nb_placesdate_reservationlibelle                 heure       date          prix   
-----------------------------------------------------------------------------------------------------------------
Dupond        Henry            2      2017-01-20    Le lac des cygnes       19:15:00    2017-02-12    150    
Dupond        Henry            2      2017-01-20    Le lac des cygnes       19:15:00    2017-02-12    150    
Dupond        Henry            2      2017-02-12    Le lac des cygnes       19:15:00    2017-02-12    150    
Dupond        Henry            4      2017-05-02    La croisi�re s'amuse    19:50:00    2017-04-18    60     
Dupond        Henry            2      2017-05-16    Le cirque de gui        21:00:00    2017-05-03    120    
Dupond        Henry            2      2017-03-14    Le lac des cygnes       19:15:00    2017-02-12    150    
Dupond        Henry            5      2017-03-14    Le lac des cygnes       19:15:00    2017-02-12    100    
Dupond        Henry            4      2017-03-14    Casse Noisettes         20:30:52    2017-03-13    55     
Dupond        Henry            6      2017-03-14    Le lac des cygnes       19:30:00    2017-02-20    130    
Dupond        Henry            2      2017-01-20    Le lac des cygnes       19:15:00    2017-02-12    150    
Dupond        Henry            2      2017-01-20    Le lac des cygnes       19:15:00    2017-02-12    150    
Dupond        Henry            1      2017-02-02    Le lac des cygnes       20:00:00    2017-03-16    67     
Dupond        Henry            2      2017-01-20    Le lac des cygnes       19:15:00    2017-02-12    150    
Dupond        Henry            2      2017-01-20    Le lac des cygnes       19:15:00    2017-02-12    150    
Dupond        Henry            2      2017-01-20    Le lac des cygnes       19:15:00    2017-02-12    150    
Dupond        Henry            1      2017-03-08    Le lac des cygnes       19:15:00    2017-02-12    150    
Dupond        Henry            2      2017-01-20    Le lac des cygnes       19:15:00    2017-02-12    150    
Dupond        Henry            2      2017-01-20    Le lac des cygnes       19:15:00    2017-02-12    150    
Dupond        Henry            3      2017-03-12    Le cirque de gui        21:00:00    2017-05-02    45     
Ronron        Fred             1      2017-02-12    Le lac des cygnes       19:15:00    2017-02-12    150    
Ronron        Fred             5      2017-04-18    La croisi�re s'amuse    15:45:00    2017-04-15    110    
Ronron        Fred             2      2017-03-16    Casse Noisettes         20:30:00    2017-03-14    90     
Ronron        Fred             3      2017-06-05    West Side Story         20:30:00    2017-05-16    75     
Gaulois       Ast�rix          2      2017-03-13    Le lac des cygnes       19:30:00    2017-02-20    130    
Gaulois       Ast�rix          4      2017-03-13    Le lac des cygnes       19:30:00    2017-02-20    80     
Gagarine      Youri            3      2017-05-02    La croisi�re s'amuse    19:50:00    2017-04-18    90     
Gagarine      Youri            5      2017-03-14    Casse Noisettes         20:30:52    2017-03-13    55     
Lagaffe       Gaston           2      2017-05-12    Le lac des cygnes       20:00:00    2017-03-16    67     
Lagaffe       Gaston           2      2017-04-15    La croisi�re s'amuse    18:15:00    2017-05-12    60     
Brassens      Georges          4      2017-03-14    Casse Noisettes         20:30:52    2017-03-13    95     
Brassens      Georges          2      2017-04-15    La croisi�re s'amuse    18:15:00    2017-05-12    60     
Mortimer      Blake            6      2017-05-16    Le cirque de gui        21:00:00    2017-05-03    80     
Mortimer      Blake            4      2017-04-18    La croisi�re s'amuse    15:45:00    2017-04-15    110    
Haddock       Capitaine        2      2017-02-12    Le lac des cygnes       19:15:00    2017-02-12    150    
Haddock       Capitaine        7      2017-04-18    La croisi�re s'amuse    15:45:00    2017-04-15    70     
Haddock       Capitaine        5      2017-05-02    La croisi�re s'amuse    19:50:00    2017-04-18    60     
Lucas         Georges          1      2017-02-12    Le lac des cygnes       19:15:00    2017-02-12    150    
Lucas         Georges          1      2017-05-03    Le cirque de gui        21:00:00    2017-05-02    45     
Moy�          Lucas            3      2017-05-16    Le cirque de gui        21:00:00    2017-05-03    120    
Moy�          Lucas            2      2017-05-16    Le cirque de gui        21:00:00    2017-05-03    80     
Moy�          Lucas            2      2017-05-16    Le cirque de gui        21:00:00    2017-05-03    120    
Moy�          Lucas            2      2017-05-16    Le cirque de gui        21:00:00    2017-05-03    120    
Moy�          Lucas            2      2017-05-16    Le cirque de gui        21:00:00    2017-05-03    80     
Moy�          Lucas            3      2017-06-05    West Side Story         20:30:00    2017-05-16    75     
Proye         Elo�se           3      2017-03-14    Casse Noisettes         20:30:52    2017-03-13    55     
Hamel         Margot           4      2017-03-15    Le lac des cygnes       19:30:00    2017-02-20    130    
Hamel         Margot           4      2017-03-15    Le lac des cygnes       19:30:00    2017-02-20    130    
Hamel         Margot           5      2017-03-15    Le lac des cygnes       19:15:00    2017-02-12    150    
Leichtnam     Oph�lia          4      2017-03-14    Le lac des cygnes       19:15:00    2017-02-12    100    
Leichtnam     Oph�lia          4      2017-03-15    Le lac des cygnes       19:15:00    2017-02-12    100    
Dupond        Marie            5      2017-03-14    Casse Noisettes         20:30:52    2017-03-13    95     
Greff         Juliane          110    2017-03-15    Le lac des cygnes       19:15:00    2017-02-12    150    
Greff         Juliane          68     2017-03-15    Le lac des cygnes       19:15:00    2017-02-12    150    
Smith         John             4      2017-03-15    Le lac des cygnes       19:30:00    2017-02-20    130    
Laigle        Amandine         3      2017-07-17    Le cirque de gui        21:00:00    2017-05-03    120    
Abadie        Camille          2      2017-03-15    Le cirque de gui        21:00:00    2017-05-03    80     
Chatue        Clemence         2      2017-04-14    West Side Story         20:15:00    2017-06-05    60     
pirot         julie            2      2017-03-28    Le cirque de gui        21:00:00    2017-05-02    45     
Deterne       Manon            2      2017-01-21    West Side Story         20:30:00    2017-05-16    100    
Bacouet       Lily             6      2017-01-21    La croisi�re s'amuse    15:45:00    2017-04-15    110    
Grondin       Megane           3      2017-01-22    Le lac des cygnes       20:00:00    2017-03-16    67     
Grondin       Megane           12     2017-01-24    Le cirque de gui        21:00:00    2017-05-03    120    
Allard        Benjamin         10     2017-01-23    Le lac des cygnes       20:00:00    2017-03-16    70     
Allard        Benjamin         4      2017-01-24    Casse Noisettes         20:30:00    2017-03-14    90     
Alonso        Fleur            3      2017-01-23    Le lac des cygnes       19:30:00    2017-02-20    130    
Jacopin       Eliott           4      2017-01-24    La croisi�re s'amuse    15:45:00    2017-04-15    110    
Gaudfernau    Fleur            6      2017-01-24    Casse Noisettes         20:30:00    2017-03-14    90     
Batot         Ga�lle           5      2017-01-25    Casse Noisettes         20:30:52    2017-03-13    95     
Cavaille      Nicolas          4      2017-01-26    West Side Story         20:30:00    2017-05-16    100    
Leitchman     Ophelia          5      2017-01-27    Le cirque de gui        21:00:00    2017-05-02    45     
Martin        Jean-Pierre      4      2017-03-05    Le lac des cygnes       19:15:00    2017-02-12    150    
Martin        Jean-Pierre      1      2017-03-08    Le lac des cygnes       19:15:00    2017-02-12    150    
Ouglan        Jean             2      2017-03-14    Le lac des cygnes       19:15:00    2017-02-12    150    
huon          charles          2      2017-03-14    Le lac des cygnes       19:15:00    2017-02-12    150    
gaudin        maelle           2      2017-03-15    Le lac des cygnes       19:15:00    2017-02-12    150    
gaudin        maelle           6      2017-03-15    Le lac des cygnes       19:15:00    2017-02-12    150    
Durant        Martin           2      2017-03-15    Le lac des cygnes       19:15:00    2017-02-12    150    
Dupond        Henry            2      2017-03-15    Le lac des cygnes       19:30:00    2017-02-20    130    
Durand        Fran�ois         2      2017-03-15    Le cirque de gui        21:00:00    2017-05-03    120    
Dupond        Henry            2      2017-01-20    Le lac des cygnes       19:15:00    2017-02-12    150    
Peras         Marie            4      2017-01-20    Le lac des cygnes       19:15:00    2017-02-12    150    
Leveque       Juliette         1      2017-01-25    Le lac des cygnes       19:15:00    2017-02-12    150    
Michel        Bruno            4      2017-01-26    Le lac des cygnes       19:15:00    2017-02-12    150    
Hartmann      Pauline          4      2017-03-12    Le lac des cygnes       19:15:00    2017-02-12    150    
Dumas         Alexandra        1      2017-03-04    Le lac des cygnes       19:15:00    2017-02-12    150    
Dumas         Alexandra        1      2017-03-08    Casse Noisettes         20:30:00    2017-03-14    65     
Dumas         Alexandra        3      2017-03-07    Le lac des cygnes       19:15:00    2017-02-12    100    
Poquelin      Jean-Baptiste    3      2017-03-14    Le lac des cygnes       19:15:00    2017-02-12    150    
Bramons       Cyrano           6      2017-03-12    Le lac des cygnes       19:15:00    2017-02-12    150    
Georges       Remi             2      2017-03-13    Le lac des cygnes       19:15:00    2017-02-12    180    
Giraud        Remi             2      2017-03-12    Le lac des cygnes       19:15:00    2017-02-12    100    
Dollars       Avida            11     2017-03-06    Le lac des cygnes       19:15:00    2017-02-12    180    
Minioves      Marie            2      2017-03-15    Le lac des cygnes       19:15:00    2017-02-12    180    
