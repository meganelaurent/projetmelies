# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 00:06:38 2018

@author: Megane Laurent
"""

import sqlite3
import tkinter
import matplotlib
import matplotlib.pyplot as plt
from pylab import bar, xticks, show
from AgroPythia_v2 import *
from agro_GUI.agro_widgets import *

def connexion_base_de_donnees() :
    """
    Déterminer une routine de connexion à la base de données spectacles.sqlite située dans le dossier bdd
    """
    #bloc_instructions
    "Chemin relatif de la base de données"
    chemin_bdd = "bdd/spectacles.sqlite"
    
    "Connexion à la base de données"
    try :
        connexion = sqlite3.connect(chemin_bdd)
        return connexion
    except Error as e :
        print(e)
        
    return None

def acces_accueil() :
    """
    Accéder à la fenêtre d'accueil
    """
    #bloc_instructions
    construire_contenu_fenetre("accueil")
    
def acces_consultation() :
    """
    Accéder à la fenêtre de consultation
    """
    #bloc_instructions
    construire_contenu_fenetre("consultation")
    
def acces_modification() :
    """
    Accéder à la fenêtre de modification
    """
    #bloc_instructions
    construire_contenu_fenetre("modification")
    
def acces_ajout() :
    """
    Accéder à la fenêtre d'ajout
    """
    #bloc_instructions
    construire_contenu_fenetre("ajout")
    
def acces_suppression() :
    """
    Accéder à la fenêtre de suppression
    """
    #bloc_instructions
    construire_contenu_fenetre("suppression")
    
def acces_statistiques() :
    """
    Accéder à la fenêtre des statistiques
    """
    #bloc_instructions
    construire_contenu_fenetre("statistiques")

def construire_contenu_fenetre(rubrique) :
    """
    Construction du contenu à afficher en fonction de la rubrique demandée
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.grid()
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de la page d'accueil"
    if(rubrique == "accueil") :
        fenetre_application.inserer_titre("Accueil", 2)
        fenetre_application.inserer_image("img/rideau.png")
        fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    "Affichage de la page de consultation"
    if(rubrique == "consultation") :
        liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
        liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
        fenetre_application.inserer_titre("Consultation", 2)
        fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
        fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
        fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    "Affichage de la page de modification"
    if(rubrique == "modification") :
        liste_labels_modification = ["Modifier représentation", "Modifier places", "Modifier clients"]
        liste_actions_modification = [modifier_representation, modifier_places, modifier_clients]
        fenetre_application.inserer_titre("Modification", 2)
        fenetre_application.inserer_text("Fonctionnalités disponibles : Modifier représentation, modifier places, modifier clients.")
        fenetre_application.inserer_list_boutons(liste_labels_modification, liste_actions_modification)
        fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    "Affichage de la page d'ajout"
    if(rubrique == "ajout") :
        liste_labels_ajout = ["Ajouter un spectacle", "Ajouter une représentation", "Ajouter un client"]
        liste_actions_ajout = [ajouter_spectacle, ajouter_representation, ajouter_client]
        fenetre_application.inserer_titre("Ajout", 2)
        fenetre_application.inserer_text("Fonctionnalités disponibles : Ajouter un spectacle, ajouter une représentation, ajouter un client.")
        fenetre_application.inserer_list_boutons(liste_labels_ajout, liste_actions_ajout)
        fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    "Affichage de la page de suppression"
    if(rubrique == "suppression") :
        liste_labels_suppression = ["Supprimer une représentation", "Supprimer une réservation", "Clients"]
        liste_actions_suppression = [supprimer_representation, supprimer_reservation, supprimer_client]
        fenetre_application.inserer_titre("Suppression", 2)
        fenetre_application.inserer_text("Fonctionnalités disponibles : Supprimer une représentation, supprimer une réservation, supprimer un client.")
        fenetre_application.inserer_list_boutons(liste_labels_suppression, liste_actions_suppression)
        fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
        
    "Affichage de la page des statistiques"
    if(rubrique == "statistiques") :
        liste_labels_statistiques = ["Statistiques clients", "Statistiques spectacles", "Taux de réservation en fonction du mois", "Classement des spectacles les plus réservés"]
        liste_actions_statistiques = [statistiques_clients, statistiques_spectacles, taux_reservation, classement_spectacles]
        fenetre_application.inserer_titre("Statistiques", 2)
        fenetre_application.inserer_text("Fonctionnalités disponibles : Statistiques clients, statistiques spectacles, taux de réservation en fonction du mois, classement des spectacles les plus réservés")
        fenetre_application.inserer_list_boutons(liste_labels_statistiques, liste_actions_statistiques)
        fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    "S'assurer que l'affichage des fenêtres soit correcte"
    fenetre_application.manage_view()
    
    return fenetre_application

def afficher_fenetre(contenu_fenetre) :
    """
    Déterminer une routine d'affichage d'une fenêtre de l'application
    """
    #bloc_instructions
    contenu_fenetre.show()
    
def construire_liste_boutons() :
    """
    Construction d'une liste de boutons standard pour toutes les pages
    """
    #bloc_instructions
    liste_labels = ["Accueil", "Consultation", "Modification", "Ajout", "Suppression", "Statistiques"]
    liste_actions = [acces_accueil, acces_consultation, acces_modification, acces_ajout, acces_suppression, acces_statistiques]
    liste_boutons = [liste_labels, liste_actions]
    
    return liste_boutons

def recuperer_resultat_requete(stockage_de_requete):
    """
    Déterminer une routine d'affichage d'une requête SQL
    """
    #bloc_instructions
    liste = stockage_de_requete.fetchall()
    
    return liste

def consulter_fonctionnalite(fonctionnalite, offset="0") :
    """
    Déterminer une routine de consultation de table pour chaque fonctionnalité. Le paramètre offset permet de créer des pages limitées en nombre d'information.
    """
    #bloc_instructions
    "Lister les caractéristiques des spectacles"
    if(fonctionnalite == "lister_les_caracteristiques_des_spectacles") :
        requete = "SELECT libelle, site_web, date, heure FROM spectacle AS s, representation AS r WHERE r.id_spectacle = s.id ORDER BY s.id LIMIT 20 OFFSET " + str(offset)
    
    "Lister les places disponibles"
    if(fonctionnalite == "lister_les_places_disponibles") :
        requete = "SELECT libelle, date, heure, type_place, prix, nb_places FROM spectacle AS s, representation AS r, place AS p WHERE p.id_representation = r.id AND r.id_spectacle = s.id ORDER BY s.id LIMIT 20 OFFSET " + str(offset)
    
    "Lister les réservations des clients"
    if(fonctionnalite == "quel_client_a_reserve_quoi") :
        requete = "SELECT nom, prenom, resa.nb_places, date_reservation, libelle, heure, date, prix FROM client AS c, reservation AS resa, place AS p, representation AS r, spectacle AS s WHERE resa.id_client = c.id AND resa.id_place = p.id AND p.id_representation = r.id AND r.id_spectacle = s.id ORDER BY c.id LIMIT 20 " + "OFFSET " + str(offset)
    
    "Lister les représentations en vue d'une modification"
    if(fonctionnalite == "modifier_une_representation") :
        requete = "SELECT r.id, s.libelle, r.date, r.heure FROM spectacle AS s, representation as r WHERE s.id = r.id_spectacle"
        
    "Lister les places en vue d'une modification"
    if(fonctionnalite == "modifier_une_place") :
        requete = "SELECT p.id, s.libelle, r.date, r.heure, p.type_place, p.prix FROM spectacle AS s, representation AS r, place AS p WHERE s.id = r.id_spectacle AND r.id = p.id_representation ORDER BY p.id"
    
    "Lister les clients en vue d'une modification"
    if(fonctionnalite == "modifier_un_client") :
        requete = "SELECT * FROM client ORDER BY id"
    
    "Lister les spectacles en vue d'en ajouter"
    if(fonctionnalite == "ajouter_un_spectacle") :
        requete = "SELECT * FROM spectacle ORDER BY id DESC LIMIT 5"
    
    "Lister les représentations en vue d'en ajouter"
    if(fonctionnalite == "lister_representation") :
        requete = "SELECT r.id, r.date, r.heure, s.libelle FROM spectacle AS s, representation AS r WHERE s.id = r.id_spectacle ORDER BY r.id DESC LIMIT 5"
    
    "Lister les spectacles pour savoir laquelle ajouter en représentation"
    if(fonctionnalite == "ajouter_une_representation") :
        requete = "SELECT id, libelle FROM spectacle ORDER BY libelle"
    
    "Lister les clients en vue d'en ajouter"
    if(fonctionnalite == "ajouter_un_client") :
        requete = "SELECT * FROM client ORDER BY id DESC LIMIT 5"
    
    "Lister les représentations en vue d'en supprimer"
    if(fonctionnalite == "supprimer_une_representation") :
        requete = "SELECT r.id, s.libelle, r.date, r.heure FROM spectacle AS s, representation AS r WHERE r.id_spectacle = s.id ORDER BY r.id"
    
    "Lister les réservations en vue d'en supprimer"
    if(fonctionnalite == "supprimer_une_reservation") :
        requete = "SELECT resa.id, c.nom, c.prenom, resa.nb_places, p.prix, r.date, r.heure, s.libelle FROM spectacle AS s, representation AS r, place AS p, reservation AS resa, client AS c WHERE s.id = r.id_spectacle AND r.id = p.id_representation AND p.id = resa.id_place AND resa.id_client = c.id"
    
    "Lister les clients en vue d'en supprimer"
    if(fonctionnalite == "supprimer_un_client") :
        requete = "SELECT * FROM client ORDER BY id"
        
    return requete

def consulter_spectacles() :
    """
    Affiche un tableau des spectacles
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    global offset_consulter_spectacles
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    offset_consulter_spectacles = 0
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des spectacles"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Caractéristiques des spectacles", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
                                           
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("lister_les_caracteristiques_des_spectacles", offset_consulter_spectacles))
    
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Libellé", "Site Web", "Date", "Heure"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_spectacles_precedent, consulter_spectacles_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def consulter_spectacles_precedent() :
    """
    Affiche un tableau des spectacles
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_spectacles
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des spectacles"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Caractéristiques des spectacles", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")

    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Prévoir un offset de 20 éléments en moins pour la page précédentes"
    offset_consulter_spectacles = offset_consulter_spectacles - 20
    
    "Si l'offset est inférieur à 0, remettre à 0"
    if(offset_consulter_spectacles < 0) :
        offset_consulter_spectacles = 0
    
    "Utilisation de l'offset généré pour déterminer le point de départ de l'affichage"
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("lister_les_caracteristiques_des_spectacles", offset_consulter_spectacles))
    
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Libellé", "Site Web", "Date", "Heure"]
    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_spectacles_precedent, consulter_spectacles_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()

def consulter_spectacles_suivant() :
    """
    Affiche un tableau des spectacles
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_spectacles
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des spectacles"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Caractéristiques des spectacles", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Utilisation de l'offset généré pour déterminer le point de départ de l'affichage"
    offset_consulter_spectacles = offset_consulter_spectacles + 20
    
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("lister_les_caracteristiques_des_spectacles", offset_consulter_spectacles))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Libellé", "Site Web", "Date", "Heure"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_spectacles_precedent, consulter_spectacles_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def consulter_places() :
    """
    Affiche les places disponibles
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_places
    offset_consulter_places = 0
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des places"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Les places disponibles", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("lister_les_places_disponibles", offset_consulter_places))
    
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Libellé", "Date", "Heure", "Type_place", "Prix", "Nb_places"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_places_precedent, consulter_places_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def consulter_places_precedent() :
    """
    Affiche les places disponibles
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_places
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des places"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Les places disponibles", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Utilisation de l'offset généré pour déterminer le point de départ de l'affichage"
    offset_consulter_places = offset_consulter_places - 20
    
    "Si l'offset est inférieur à 0, remettre à 0"
    if(offset_consulter_places < 0) :
        offset_consulter_places = 0
    
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("lister_les_places_disponibles", offset_consulter_places))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Libellé", "Date", "Heure", "Type_place", "Prix", "Nb_places"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_places_precedent, consulter_places_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def consulter_places_suivant() :
    """
    Affiche les places disponibles
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_places
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des places"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Les places disponibles", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Utilisation de l'offset généré pour déterminer le point de départ de l'affichage"
    offset_consulter_places = offset_consulter_places + 20
    
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("lister_les_places_disponibles", offset_consulter_places))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Libellé", "Date", "Heure", "Type_place", "Prix", "Nb_places"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_places_precedent, consulter_places_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def consulter_clients() :
    """
    Affiche les clients
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_clients
    offset_consulter_clients = 0
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des clients"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Clients", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("quel_client_a_reserve_quoi", offset_consulter_clients))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Nom", "Prenom", "Resa.nb_places", "Date_reservation", "Libelle", "Heure", "Date", "Prix"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_clients_precedent, consulter_clients_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def consulter_clients_precedent() :
    """
    Affiche les clients
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_clients
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des clients"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Clients", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Utilisation de l'offset généré pour déterminer le point de départ de l'affichage"
    offset_consulter_clients = offset_consulter_clients - 20
    
    "Si l'offset est inférieur à 0, remettre à 0"
    if(offset_consulter_clients < 0) :
        offset_consulter_clients = 0
        
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("quel_client_a_reserve_quoi", offset_consulter_clients))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Nom", "Prenom", "Resa.nb_places", "Date_reservation", "Libelle", "Heure", "Date", "Prix"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_clients_precedent, consulter_clients_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def consulter_clients_suivant() :
    """
    Affiche les clients
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    "Variable d'offset utilisable pour le point de départ d'affichage dans la requête SQL"
    global offset_consulter_clients
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de consultation des clients"
    liste_labels_consultation = ["Caractéristiques des spectacles", "Les places disponibles", "Clients"]
    liste_actions_consultation = [consulter_spectacles, consulter_places, consulter_clients]
    fenetre_application.inserer_titre("Consultation", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Lister les caractéristiques des spectacles, lister les places disponibles, quel client a réservé quoi ?")
    fenetre_application.inserer_titre("Clients", 3)
    fenetre_application.inserer_list_boutons(liste_labels_consultation, liste_actions_consultation)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Utilisation de l'offset généré pour déterminer le point de départ de l'affichage"
    offset_consulter_clients = offset_consulter_clients + 20
    
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("quel_client_a_reserve_quoi", offset_consulter_clients))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["Nom", "Prenom", "Resa.nb_places", "Date_reservation", "Libelle", "Heure", "Date", "Prix"]

    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    liste_labels_boutons = ["Précédent", "Suivant"]
    
    "Appel des fonctions pour les pages précédentes et suivantes"
    liste_actions_boutons = [consulter_clients_precedent, consulter_clients_suivant]
    fenetre_application.inserer_list_boutons(liste_labels_boutons, liste_actions_boutons)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def modifier_representation() :
    """
    Affiche les représentations pour modification
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    fenetre_application.clear()
    
    "Suppression des anciennes données"
    fenetre_application.datas.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de modification des représentations"
    liste_labels_modification = ["Modifier représentation", "Modifier places", "Modifier clients"]
    liste_actions_modification = [modifier_representation, modifier_places, modifier_clients]
    fenetre_application.inserer_titre("Modification", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Modifier représentation, modifier places, modifier clients.")
    fenetre_application.inserer_titre("Modifier représentation", 3)
    fenetre_application.inserer_list_boutons(liste_labels_modification, liste_actions_modification)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("modifier_une_representation"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)

    fenetre_application.inserer_listebox("liste_representation", "Sélectionner une représentation, indiquer la nouvelle date et heure et appuyer sur Modifier", liste_valeurs)
    fenetre_application.inserer_calendrier("representation", "Nouvelle date de la représentation sélectionnée")
    fenetre_application.inserer_champ_text("heure", "Nouveaux horaires de la représentation sélectionnée sous format HH:MM:SS")
    fenetre_application.inserer_bouton("Modifier", modifier_representation_selectionnee)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def modifier_representation_selectionnee() :
    """
    Modifier la représentation sélectionnée
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération des données pour mise à jour"
    contenu = []
    for element in fenetre_application.datas.items() :
        contenu.append(element[1])
    
    "Récupération des données utiles"
    id_representation = contenu[0][0][0]
    date_representation = contenu[1]
    heure_representation = contenu[2]
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Modification de la représentation sélectionnée"
    stockage_de_requete.execute("UPDATE representation SET date = ?, heure = ? WHERE id = ?", (date_representation, heure_representation, id_representation,))
    
    "Validation de la modification"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page de modification des représentations"
    modifier_representation()
    
def modifier_places() :
    """
    Affiche les places pour modification
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    fenetre_application.clear()
    
    "Suppression des anciennes données"
    fenetre_application.datas.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de modification des places"
    liste_labels_modification = ["Modifier représentation", "Modifier places", "Modifier clients"]
    liste_actions_modification = [modifier_representation, modifier_places, modifier_clients]
    fenetre_application.inserer_titre("Modification", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Modifier représentation, modifier places, modifier clients.")
    fenetre_application.inserer_titre("Modifier places", 3)
    fenetre_application.inserer_list_boutons(liste_labels_modification, liste_actions_modification)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("modifier_une_place"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)

    fenetre_application.inserer_listebox("liste_representation", "Sélectionner une place, indiquer le nouveau prix en € et appuyer sur Modifier", liste_valeurs)
    fenetre_application.inserer_champ_text("prix", "Nouveau prix au format XXX")
    fenetre_application.inserer_bouton("Modifier", modifier_place_selectionnee)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def modifier_place_selectionnee() :
    """
    Modifier le prix d'une place sélectionnée
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération des données pour mise à jour"
    contenu = []
    for element in fenetre_application.datas.items() :
        contenu.append(element)
    
    "Récupération des données utiles"
    id_place = contenu[0][1][0][0]
    prix_place = contenu[1][1]
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Modifier de la représentation sélectionnée"
    stockage_de_requete.execute("UPDATE place SET prix = ? WHERE id = ?", (prix_place, id_place,))
    
    "Validation de la modification"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page de modification des places"
    modifier_places()
    
def modifier_clients() :
    """
    Affiche les clients pour modification
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    fenetre_application.clear()
    
    "Suppression des anciennes données"
    fenetre_application.datas.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de modification des clients"
    liste_labels_modification = ["Modifier représentation", "Modifier places", "Modifier clients"]
    liste_actions_modification = [modifier_representation, modifier_places, modifier_clients]
    fenetre_application.inserer_titre("Modification", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Modifier représentation, modifier places, modifier clients.")
    fenetre_application.inserer_titre("Modifier clients", 3)
    fenetre_application.inserer_list_boutons(liste_labels_modification, liste_actions_modification)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("modifier_un_client"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)

    fenetre_application.inserer_listebox("liste_representation", "Sélectionner un client, remplir son nom, prénom, email et appuyer sur Modifier", liste_valeurs)
    fenetre_application.inserer_champ_text("nom", "Nouveau Nom")
    fenetre_application.inserer_champ_text("prenom", "Nouveau Prénom")
    fenetre_application.inserer_champ_text("email", "Nouveau Mail")
    fenetre_application.inserer_bouton("Modifier", modifier_client_selectionne)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def modifier_client_selectionne() :
    """
    Modifier les coordonnées du client sélectionné
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération des données pour mise à jour"
    contenu = []
    for element in fenetre_application.datas.items() :
        contenu.append(element)
    
    "Récupération des données utiles"
    id_client = contenu[0][1][0][0]
    nom_actuel_client = contenu[0][1][0][1]
    prenom_actuel_client = contenu[0][1][0][2]
    email_actuel_client = contenu[0][1][0][3]
    
    nouveau_nom_client = contenu[1][1]
    nouveau_prenom_client = contenu[2][1]
    nouvel_email_client = contenu[3][1]
    
    "On prévoit certaines cases vides"
    if(len(nouveau_nom_client) == 0) :
        nouveau_nom_client = nom_actuel_client
    if(len(nouveau_prenom_client) == 0) :
        nouveau_prenom_client = prenom_actuel_client
    if(len(nouvel_email_client) == 0) :
        nouvel_email_client = email_actuel_client
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Modifier de la représentation sélectionnée"
    stockage_de_requete.execute("UPDATE client SET prenom = ?, nom = ?, email = ? WHERE id = ?", (nouveau_nom_client, nouveau_prenom_client, nouvel_email_client, id_client,))
    
    "Validation de la modification"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page de modification des clients"
    modifier_clients()
    
def ajouter_spectacle() :
    """
    Affiche un formulaire pour ajouter un spectacle
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    fenetre_application.clear()
    
    "Suppression des anciennes données"
    fenetre_application.datas.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface d'ajout de spectacles"
    liste_labels_ajout = ["Ajouter spectacle", "Ajouter représentation", "Ajouter client"]
    liste_actions_ajout = [ajouter_spectacle, ajouter_representation, ajouter_client]
    fenetre_application.inserer_titre("Ajout", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Ajouter spectacle, ajouter représentation, ajouter client.")
    fenetre_application.inserer_titre("Ajouter un spectacle", 3)
    fenetre_application.inserer_list_boutons(liste_labels_ajout, liste_actions_ajout)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("ajouter_un_spectacle"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["ID", "Libellé", "Site Web"]
    
    "Afficher les spectacles existants"
    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    fenetre_application.inserer_champ_text("libelle", "Libellé")
    fenetre_application.inserer_champ_text("site", "Site Web")
    fenetre_application.inserer_bouton("Ajouter", ajouter_spectacle_formulaire)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def ajouter_spectacle_formulaire() :
    """
    Ajouter le nouveau spectacle
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération des données pour mise à jour"
    contenu = []
    for element in fenetre_application.datas.items() :
        contenu.append(element)
    
    "Récupération des données utiles"
    libelle_spectacle = contenu[0][1]
    site_spectacle = contenu[1][1]
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    stockage_de_requete.execute("INSERT INTO spectacle (libelle, site_web) VALUES (?, ?)", (libelle_spectacle, site_spectacle,))
    
    "Validation de l'ajout"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page d'ajout de spectacles"
    ajouter_spectacle()
    
def ajouter_representation() :
    """
    Affiche un formulaire pour ajouter une représentation
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    fenetre_application.clear()
    
    "Suppression des anciennes données"
    fenetre_application.datas.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface d'ajout des représentation"
    liste_labels_ajout = ["Ajouter spectacle", "Ajouter représentation", "Ajouter client"]
    liste_actions_ajout = [ajouter_spectacle, ajouter_representation, ajouter_client]
    fenetre_application.inserer_titre("Ajout", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Ajouter spectacle, ajouter représentation, ajouter client.")
    fenetre_application.inserer_titre("Ajouter représentation", 3)
    fenetre_application.inserer_list_boutons(liste_labels_ajout, liste_actions_ajout)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("lister_representation"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["ID", "Date", "Heure", "Libellé"]
    
    "Afficher les représentations existantes"
    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    
    "Afficher les spectacles existants"
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("ajouter_une_representation"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    fenetre_application.inserer_listebox("liste_spectacle", "Sélectionner le spectacle qui sera représenté", liste_valeurs)
    
    fenetre_application.inserer_calendrier("date", "Date de la nouvelle représentation")
    fenetre_application.inserer_champ_text("heure", "Heure de la nouvelle représentation au format HH:MM:SS")
    fenetre_application.inserer_bouton("Ajouter", ajouter_representation_formulaire)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def ajouter_representation_formulaire() :
    """
    Ajouter la nouvelle représentation
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération des données pour mise à jour"
    contenu = []
    for element in fenetre_application.datas.items() :
        contenu.append(element)
    
    "Récupération des données utiles"
    id_spectacle = contenu[0][1][0][0]
    date_representation = contenu[1][1]
    heure_representation = contenu[2][1]
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete.execute("INSERT INTO representation (id_spectacle, date, heure) VALUES (?, ?, ?)", (id_spectacle, date_representation, heure_representation,))
    
    "Validation de l'ajout"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page d'ajout des représentations"
    ajouter_representation()
    
def ajouter_client() :
    """
    Affiche un formulaire pour ajouter un client
    """
    #bloc_instructions
    "Nettoyage de l'affichage"
    global fenetre_application
    
    fenetre_application.clear()
    
    "Suppression des anciennes données"
    fenetre_application.datas.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface d'ajout des clients"
    liste_labels_ajout = ["Ajouter spectacle", "Ajouter représentation", "Ajouter client"]
    liste_actions_ajout = [ajouter_spectacle, ajouter_representation, ajouter_client]
    fenetre_application.inserer_titre("Ajout", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Ajouter spectacle, ajouter représentation, ajouter client.")
    fenetre_application.inserer_titre("Ajouter client", 3)
    fenetre_application.inserer_list_boutons(liste_labels_ajout, liste_actions_ajout)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("ajouter_un_client"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)
    liste_entetes = ["ID", "Nom", "Prénom", "Email"]
    
    fenetre_application.inserer_tableau(liste_valeurs, liste_entetes)
    fenetre_application.inserer_champ_text("nom", "Nom du nouveau client")
    fenetre_application.inserer_champ_text("prenom", "Prénom du nouveau client")
    fenetre_application.inserer_champ_text("email", "Email du nouveau client")
    fenetre_application.inserer_bouton("Ajouter", ajouter_client_formulaire)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def ajouter_client_formulaire() :
    """
    Ajouter le nouveau client
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération des données pour mise à jour"
    contenu = []
    for element in fenetre_application.datas.items() :
        contenu.append(element)
    
    "Récupération des données utiles"
    nom_client = contenu[0][1]
    prenom_client = contenu[1][1]
    email_client = contenu[2][1]
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete.execute("INSERT INTO client (nom, prenom, email) VALUES (?, ?, ?)", (nom_client, prenom_client, email_client,))
    
    "Validation de l'ajout"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page d'ajout des clients"
    ajouter_client()
    
def supprimer_representation() :
    """
    Supprimer une représentation
    """
    #bloc_instructions
    global fenetre_application
    
    fenetre_application.clear()
    
    "Suppression des anciennes données"
    fenetre_application.datas.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de suppression d'une représentation"
    liste_labels_suppression = ["Supprimer une représentation", "Supprimer une réservation", "Supprimer un client"]
    liste_actions_suppression = [supprimer_representation, supprimer_reservation, supprimer_client]
    fenetre_application.inserer_titre("Suppression", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Supprimer une représentation, supprimer une réservation, supprimer un client.")
    fenetre_application.inserer_titre("Supprimer une représentation", 3)
    fenetre_application.inserer_list_boutons(liste_labels_suppression, liste_actions_suppression)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("supprimer_une_representation"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)

    fenetre_application.inserer_listebox("liste_representation", "Sélectionner une représentation et appuyer sur Valider pour la supprimer", liste_valeurs)
    fenetre_application.inserer_bouton("Supprimer", supprimer_representation_selectionnee)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def supprimer_representation_selectionnee() :
    """
    Supprimer une représentation sélectionnée dans le menu déroulant en s'assurant d'avoir supprimé sa les places et les réservations au préalable afin de satisfaire les contraintes de clés étrangères
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération de l'ID de la représentation à supprimer"
    for element in fenetre_application.datas.items() :
        id_representation_a_supprimer = str(element[1][0][0])
        
    "Il faut au préalable supprimer les réservations en lien avec les places qui sont en lien avec la représentation"
    "On liste toutes les places ayant pour clé étrangère la représentation qu'on souhaite supprimer"
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Quand une réservation existe, on supprime d'abord la réservation"
    try :
        stockage_de_requete.execute("SELECT id FROM place WHERE id_representation = ?", id_representation_a_supprimer)
        
        for id_place in stockage_de_requete.fetchall() :
            stockage_de_requete.execute("DELETE FROM reservation WHERE id_place = ?", (str(id_place[0]),))
        
        "Valider la requête de suppression des réservations"
        connexion.commit()
        
        "On supprime les places en rapport avec les réservations précédentes"
        stockage_de_requete.execute("SELECT id FROM place WHERE id_representation = ?", id_representation_a_supprimer)
        for id_place in stockage_de_requete.fetchall() :
            stockage_de_requete.execute("DELETE FROM place WHERE id_representation = ?", (id_representation_a_supprimer,))
            
        "Valider la requête de suppression des places"
        connexion.commit()
        
        "On supprimer les réservations en rapport avec les places précédentes"
        stockage_de_requete.execute("DELETE FROM representation WHERE id = ?", (id_representation_a_supprimer,))
            
        "Valider la requête de suppression des places"
        connexion.commit()
    except :
        "Quand aucune réservation n'existe, on supprime directement les places puis la représentation"
        stockage_de_requete.execute("DELETE FROM place WHERE id_representation = ?", (id_representation_a_supprimer,))
        stockage_de_requete.execute("DELETE FROM representation WHERE id = ?", (id_representation_a_supprimer,))
        connexion.commit()
    
    connexion.close()
    
    "Retour sur la page de suppression des représentations"
    supprimer_representation()
    
def supprimer_reservation() :
    """
    Supprimer une réservation
    """
    #bloc_instructions
    global fenetre_application
    
    fenetre_application.clear()
    
    "Suppression des anciennes données"
    fenetre_application.datas.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de suppression d'une représentation"
    liste_labels_suppression = ["Supprimer une représentation", "Supprimer une réservation", "Supprimer un client"]
    liste_actions_suppression = [supprimer_representation, supprimer_reservation, supprimer_client]
    fenetre_application.inserer_titre("Suppression", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Supprimer une représentation, supprimer une réservation, supprimer un client.")
    fenetre_application.inserer_titre("Supprimer une réservation", 3)
    fenetre_application.inserer_list_boutons(liste_labels_suppression, liste_actions_suppression)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à la base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("supprimer_une_reservation"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)

    fenetre_application.inserer_listebox("liste_representation", "Sélectionner une réservation et appuyer sur Supprimer pour la supprimer", liste_valeurs)
    fenetre_application.inserer_bouton("Supprimer", supprimer_reservation_selectionnee)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def supprimer_reservation_selectionnee() :
    """
    Supprimer la réservation sélectionnée du menu déroulant
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération de l'ID de la représentation à supprimer"
    for element in fenetre_application.datas.items() :
        id_reservation_a_supprimer = str(element[1][0][0])
        
    "Connexion à la base de données"    
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete.execute("DELETE FROM reservation WHERE id = ?", (id_reservation_a_supprimer,))
    
    "Valider la requête"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page de suppression des réservations"
    supprimer_reservation()
    
def supprimer_client() :
    """
    Supprimer un client sélectionné dans le menu déroulant en s'assurant d'avoir supprimé sa réservation au préalable afin de satisfaire la contrainte de clé étrangère
    """
    #bloc_instructions
    global fenetre_application
    
    fenetre_application.clear()
    
    "Suppression des anciennes données"
    fenetre_application.datas.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface de suppression des clients"
    liste_labels_suppression = ["Supprimer une représentation", "Supprimer une réservation", "Supprimer un client"]
    liste_actions_suppression = [supprimer_representation, supprimer_reservation, supprimer_client]
    fenetre_application.inserer_titre("Suppression", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Supprimer une représentation, supprimer une réservation, supprimer un client.")
    fenetre_application.inserer_titre("Supprimer un client", 3)
    fenetre_application.inserer_list_boutons(liste_labels_suppression, liste_actions_suppression)
    
    "Affichage du tableau"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    "Connexion à une base de données"
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    stockage_de_requete = stockage_de_requete.execute(consulter_fonctionnalite("supprimer_un_client"))
    liste_valeurs = recuperer_resultat_requete(stockage_de_requete)

    fenetre_application.inserer_listebox("liste_client", "Sélectionner un client et appuyer sur Supprimer pour le supprimer", liste_valeurs)
    fenetre_application.inserer_bouton("Supprimer", supprimer_client_selectionnee)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
    connexion.close()
    
def supprimer_client_selectionnee() :
    """
    Supprimer le client sélectionné du menu déroulant
    """
    #bloc_instructions
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    "Récupération de l'ID de la représentation à supprimer"
    for element in fenetre_application.datas.items() :
        id_client_a_supprimer = str(element[1][0][0])
        
    "Connexion à la base de données"    
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    "Suppression des réservations effectuées par le client avant de supprimer le client de la liste"
    stockage_de_requete.execute("DELETE FROM reservation WHERE id_client = ?", (id_client_a_supprimer,))
    stockage_de_requete.execute("DELETE FROM client WHERE id = ?", (id_client_a_supprimer,))
    
    "Valider la requête"
    connexion.commit()
    
    connexion.close()
    
    "Retour sur la page de suppression des clients"
    supprimer_client()
    
def statistiques_clients() :
    """
    Affichage d'une recherche par client
    """
    #bloc_instructions
    global fenetre_application
    
    fenetre_application.clear()
    
    "Suppression des anciennes données"
    fenetre_application.datas.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
                                           
    liste_labels_statistiques = ["Statistiques client", "Statistiques spectacle", "Taux de réservation en fonction du mois", "Classement des spectacles les plus réservés"]
    liste_actions_statistiques = [statistiques_clients, statistiques_spectacles, taux_reservation, classement_spectacles]
    fenetre_application.inserer_titre("Statistiques", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Statistiques client, statistiques spectacles, taux de réservation en fonction du mois, classement des spectacles les plus réservés")
    fenetre_application.inserer_list_boutons(liste_labels_statistiques, liste_actions_statistiques)
    
    "Affichage de l'interface des statistiques"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    fenetre_application.inserer_titre("Statistiques client",1)
    fenetre_application.inserer_champ_text("client","Saisissez le nom du client dont vous voulez connaitre les statistiques", width = 15)
    fenetre_application.inserer_bouton("Voir statistiques", recuperer_reservations_depenses_clients)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
def recuperer_reservations_depenses_clients() :
    """
    Affichage des réservations et des dépenses en fonction du client recherché
    """
    #bloc_instructions
    global fenetre_application
    
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    for element in fenetre_application.datas.items() :
        client = element[1]
        
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
                                           
    liste_labels_statistiques = ["Statistiques client", "Statistiques spectacle", "Taux de réservation en fonction du mois", "Classement des spectacles les plus réservés"]
    liste_actions_statistiques = [statistiques_clients, statistiques_spectacles, taux_reservation, classement_spectacles]
    fenetre_application.inserer_titre("Statistiques", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Statistiques client, statistiques spectacle, taux de réservation en fonction du mois, classement des spectacles les plus réservés")
    fenetre_application.inserer_list_boutons(liste_labels_statistiques, liste_actions_statistiques)
    
    "Affichage de l'interface des statistiques"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    fenetre_application.inserer_titre("Statistiques client",1)
    fenetre_application.inserer_champ_text('client',"Saisissez le nom du client dont vous voulez connaitre les statistiques", width = 15)
    fenetre_application.inserer_bouton("Voir statistiques", recuperer_reservations_depenses_clients)
    
    "Récupérer le nombre de place total réservée par le client recherché"
    nombre_de_places_reservees = recuperer_nombre_de_places_reservees(client)
    
    "Récupérer la somme totale dépensée par le client recherché"
    somme_depensee = recuperer_somme_depensee(client)
    
    "Afficher le nombre de places réservées"
    fenetre_application.inserer_text("Voici le nombre de places réservées par ce client : " + str(nombre_de_places_reservees))
    
    "Afficher la somme totale dépensée"
    fenetre_application.inserer_text("Voici la somme totale dépensée par ce client : " + str(somme_depensee) + "€")
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
def recuperer_nombre_de_places_reservees(client) :
    """
    Récupération des réservations en fonction du client recherché
    """
    #bloc_instructions
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    requete = "SELECT SUM(nb_places) FROM reservation, client WHERE client.id = reservation.id_client AND client.nom = ?"
    stockage_de_requete.execute(requete,[client])
    
    nombre_de_places_reservees = stockage_de_requete.fetchall()
    nombre_de_places_reservees = nombre_de_places_reservees[0][0]
    
    connexion.close()
    
    return nombre_de_places_reservees

def recuperer_somme_depensee(client) :
    """
    Récupération des sommes dépensées en fonction du client recherché
    """
    #bloc_instructions
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    requete = "SELECT SUM(prix) FROM place, client,reservation WHERE reservation.id_place = place.id AND client.id = reservation.id_client AND client.nom = ?"
    stockage_de_requete.execute(requete,[client])
    
    somme_depensee = stockage_de_requete.fetchall()
    somme_depensee = somme_depensee[0][0]
    
    return somme_depensee
    
def statistiques_spectacles() :
    """
    Affichage d'une recherche par spectacle
    """
    #bloc_instructions
    global fenetre_application
    
    fenetre_application.clear()
    
    "Suppression des anciennes données"
    fenetre_application.datas.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
                                           
    liste_labels_statistiques = ["Statistiques client", "Statistiques spectacles", "Taux de réservation en fonction du mois", "Classement des spectacles les plus réservés"]
    liste_actions_statistiques = [statistiques_clients, statistiques_spectacles, taux_reservation, classement_spectacles]
    fenetre_application.inserer_titre("Statistiques", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Statistiques client, statistiques spectacle, taux de réservation en fonction du mois, classement des spectacles les plus réservés")
    fenetre_application.inserer_list_boutons(liste_labels_statistiques, liste_actions_statistiques)
    
    "Affichage de l'interface des statistiques"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    fenetre_application.inserer_titre("Statistiques spectacle",1)
    fenetre_application.inserer_champ_text("client","Saisissez le nom du spectacle dont vous voulez connaître les statistiques", width = 15)
    fenetre_application.inserer_bouton("Voir statistiques", recuperer_representation_reservations_recette_spectacles)
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
def recuperer_representation_reservations_recette_spectacles() :
    """
    Récupération des représentation, réservation et recette en fonction du spectacle recherché
    """
    #bloc_instructions
    global fenetre_application
    
    "Récupération des données saisies"
    fenetre_application.get_datas()
    
    for element in fenetre_application.datas.items() :
        spectacle = element[1]
        
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
                                           
    liste_labels_statistiques = ["Statistiques client", "Statistiques spectacle", "Taux de réservation en fonction du mois", "Classement des spectacles les plus réservés"]
    liste_actions_statistiques = [statistiques_clients, statistiques_spectacles, taux_reservation, classement_spectacles]
    fenetre_application.inserer_titre("Statistiques", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Statistiques client, statistiques spectacle, taux de réservation en fonction du mois, classement des spectacles les plus réservés")
    fenetre_application.inserer_list_boutons(liste_labels_statistiques, liste_actions_statistiques)
    
    "Affichage de l'interface des statistiques"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    
    fenetre_application.inserer_titre("Statistiques spectacle",1)
    fenetre_application.inserer_champ_text('client',"Saisissez le nom du spectacle dont vous voulez connaitre les statistiques", width = 15)
    fenetre_application.inserer_bouton("Voir statistiques", recuperer_representation_reservations_recette_spectacles)
    fenetre_application.inserer_titre(str(spectacle), 3)
    
    "Récupérer le nombre de représentation d'un spectacle"
    nombre_de_representation = recuperer_nombre_de_representation(spectacle)

    "Récupérer le nombre de réservation d'un spectacle"
    nombre_de_reservation = recuperer_nombre_de_reservation(spectacle)
    
    "Récupérer la recette d'un spectacle"
    recette = recuperer_recette(spectacle)
    
    "Afficher le nombre de représentation"
    fenetre_application.inserer_text("Voici le nombre de représentations de ce spectacle : " + str(nombre_de_representation))
    
    "Afficher le nombre de réservation"
    fenetre_application.inserer_text("Voici le nombre de places total réservées pour ce spectacle : " + str(nombre_de_reservation))
    
    "Afficher la recette"
    fenetre_application.inserer_text("Voici la recette pour ce spectacle : " + str(recette) + "€")
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)

def recuperer_nombre_de_representation(spectacle) :
    """
    Récupération du nombre de représentation en fonction du spectacle recherché
    """
    #bloc_instructions
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    requete = "SELECT COUNT(*) FROM spectacle, representation WHERE representation.id_spectacle = spectacle.id AND spectacle.libelle = ?"
    stockage_de_requete.execute(requete,[spectacle])
    
    nombre_de_representation = stockage_de_requete.fetchall()
    nombre_de_representation = nombre_de_representation[0][0]
    
    connexion.close()
    
    return nombre_de_representation

def recuperer_nombre_de_reservation(spectacle) :
    """
    Récupération du nombre de réservation en fonction du spectacle recherché
    """
    #bloc_instructions
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    requete = "SELECT SUM(reservation.nb_places) FROM place, representation, reservation, spectacle WHERE reservation.id_place = place.id AND place.id_representation = representation.id AND representation.id_spectacle = spectacle.id AND spectacle.libelle = ?"
    stockage_de_requete.execute(requete,[spectacle])
    
    nombre_de_reservation = stockage_de_requete.fetchall()
    nombre_de_reservation = nombre_de_reservation[0][0]
    
    connexion.close()
    
    return nombre_de_reservation
    
def recuperer_recette(spectacle) :
    """
    Récupération de la recette en fonction du spectacle recherché
    """
    #bloc_instructions
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    
    requete = "SELECT SUM(reservation.nb_places*prix) FROM place, representation, reservation, spectacle WHERE reservation.id_place = place.id AND place.id_representation = representation.id AND representation.id_spectacle = spectacle.id AND spectacle.libelle = ?"
    stockage_de_requete.execute(requete,[spectacle])
    
    recette = stockage_de_requete.fetchall()
    recette = recette[0][0]
    
    connexion.close()
    
    return recette
    
def taux_reservation() :
    """
    Afficher le taux de réservation par mois
    """
    #bloc_instructions
    global fenetre_application
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface des statistiques"
    liste_labels_statistiques = ["Statistiques client", "Statistiques spectacle", "Taux de réservation en fonction du mois", "Classement des spectacles les plus réservés"]
    liste_actions_statistiques = [statistiques_clients, statistiques_spectacles, taux_reservation, classement_spectacles]
    fenetre_application.inserer_titre("Statistiques", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Statistiques client, statistiques spectacle, taux de réservation en fonction du mois, classement des spectacles les plus réservés")
    fenetre_application.inserer_list_boutons(liste_labels_statistiques, liste_actions_statistiques)
    
    "Affichage de la partie graphique"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
                                           
    fenetre_application.inserer_titre("Taux de réservation (%) en fonction des mois de l'année 2017", 3)
    
    "Récupéreration du nombre de réservation totale sur une période donnée"
    nombre_total_reservations = []
    nombre_total_reservations = recuperation_nombre_total_reservations()
    
    "Récupération du nombre de place totale sur une période donnée"
    nombre_total_places = []
    nombre_total_places = recuperation_nombre_total_places()
    
    "Calcul du taux"
    taux_de_reservation = []
    taux_de_reservation = calcul_taux_reservation(nombre_total_reservations, nombre_total_places)
    
    "Dessin du graphique"
    nombre_de_barre = 12
    hauteur_des_barres = (taux_de_reservation[0], taux_de_reservation[1], taux_de_reservation[2], taux_de_reservation[3], taux_de_reservation[4], taux_de_reservation[5], taux_de_reservation[6], taux_de_reservation[7], taux_de_reservation[8], taux_de_reservation[9], taux_de_reservation[10], taux_de_reservation[11])
    position_en_x_des_barres = range(nombre_de_barre)
    largeur_des_barres = 0.5
    
    """
    Position : range de 0 à nombre de barre
    Hauteur : hauteur des barres
    Largeur d'une barre : largeur des barres
    Couleur : couleur des barres
    """
    
    bar(position_en_x_des_barres, hauteur_des_barres, largeur_des_barres, color = "r")
    
    "Initialisation d'une liste pour la légende"
    legende_axe_x = []
    
    "Attribuer une position dans l'axe des x pour chaque barre"
    for i in position_en_x_des_barres :
        legende_axe_x.append(i)
    
    "Attribution de la légende pour chaque position"
    xticks(legende_axe_x, ("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"))
    
    "Réglage de la taille du graphique"
    plt = matplotlib.pyplot.gcf()
    plt.set_size_inches(15, 8)
    
    "Génération d'une image du graphique et affichage de cette image dans l'application"
    plt.savefig("img/taux_de_reservation_mois.png")
    plt.clear()
    fenetre_application.inserer_image("img/taux_de_reservation_mois.png")
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)
    
def recuperation_nombre_total_reservations() :
    """
    Récupération des réservations totaux par mois sur une année donnée
    """
    #bloc_instructions
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    liste_reservations_par_mois = []
    
    for i in range(1, 13) :
        if(i <= 9) :
            formatage_date = '%-0' + str(i) + '-%'
        else :
            formatage_date = '%-' + str(i) + '-%'
            
        requete = "SELECT SUM(reservation.nb_places) FROM place, reservation, representation WHERE reservation.id_place = place.id AND representation.id = place.id_representation AND representation.date LIKE ?"
        stockage_de_requete.execute(requete, [formatage_date])

        for nombre_de_reservations in stockage_de_requete.fetchall() :
            if(nombre_de_reservations[0] == None) :
                liste_reservations_par_mois.append(0)
            else :
                liste_reservations_par_mois.append(nombre_de_reservations[0])
            
    connexion.close()
    
    return liste_reservations_par_mois
    
def recuperation_nombre_total_places() :
    """
    Récupération des places totales par mois sur une année donnée
    """
    #bloc_instructions
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    liste_places_par_mois = []
    
    for i in range(1, 13) :
        if(i <= 9) :
            formatage_date = '%-0' + str(i) + '-%'
        else :
            formatage_date = '%-' + str(i) + '-%'
            
        requete = "SELECT SUM(nb_places) FROM place, representation WHERE representation.id = place.id_representation AND representation.date LIKE ?"
        stockage_de_requete.execute(requete, [formatage_date])

        for nombre_de_places in stockage_de_requete.fetchall() :
            if(nombre_de_places[0] == None) :
                liste_places_par_mois.append(0)
            else :
                liste_places_par_mois.append(nombre_de_places[0])
            
    connexion.close()
    
    return liste_places_par_mois
    
def calcul_taux_reservation(nombre_total_reservations, nombre_total_places):
    """
    Calculer le taux de réservation (nombre de place réservée / nombre de place total) d'une année donnée
    """
    #bloc_instructions
    liste_taux_reservation = []
    
    for i in range(0, 12) :
        if nombre_total_reservations[i] == 0 or nombre_total_places[i] == 0 :
            taux = 0
        else :
            taux = nombre_total_reservations[i] / nombre_total_places[i]
        
        taux = round(taux*100, 2)
        liste_taux_reservation.append(taux)
        
    return liste_taux_reservation
    
def classement_spectacles() :
    """
    Afficher les spectacles les plus rentables
    """
    #bloc_instructions
    global fenetre_application
    
    fenetre_application.clear()
    
    "Affichage récurrent"
    fenetre_application.inserer_titre("Méliès Manager",1)
    liste_boutons = construire_liste_boutons()
    liste_labels = liste_boutons[0]
    liste_actions = liste_boutons[1]
    
    "Affichage de l'interface des statistiques"
    liste_labels_statistiques = ["Statistiques client", "Statistiques spectacle", "Taux de réservation en fonction du mois", "Classement des spectacles les plus réservés"]
    liste_actions_statistiques = [statistiques_clients, statistiques_spectacles, taux_reservation, classement_spectacles]
    fenetre_application.inserer_titre("Statistiques", 2)
    fenetre_application.inserer_text("Fonctionnalités disponibles : Statistiques client, statistiques spectacle, taux de réservation en fonction du mois, classement des spectacles les plus réservés")
    fenetre_application.inserer_list_boutons(liste_labels_statistiques, liste_actions_statistiques)
    
    "Affichage de la partie graphique"
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_titre("Classement des spectacles (libellés) en fonction de la recette générée (€)", 3)

    "Récupéreration des recettes de chaque spectacle"
    liste_recettes_spectacles = []
    liste_recettes_spectacles = recuperation_recettes_spectacles()
    libelle_spectacle = []
    recette_spectacle = []
    
    for element in liste_recettes_spectacles :
        libelle_spectacle.append(element[0])
        recette_spectacle.append(element[1])
    
    "Dessin du graphique"
    nombre_de_barre = 5
    hauteur_des_barres = (recette_spectacle[0], recette_spectacle[1], recette_spectacle[2], recette_spectacle[3], recette_spectacle[4])
    position_en_x_des_barres = range(nombre_de_barre)
    largeur_des_barres = 0.5
    
    """
    Position : range de 0 à nombre de barre
    Hauteur : hauteur des barres
    Largeur d'une barre : largeur des barres
    Couleur : couleur des barres
    """
    
    bar(position_en_x_des_barres, hauteur_des_barres, largeur_des_barres, color = "r")
    
    "Initialisation d'une liste pour la légende"
    legende_axe_x = []
    
    "Attribuer une position dans l'axe des x pour chaque barre"
    for i in position_en_x_des_barres :
        legende_axe_x.append(i)
    
    "Attribution de la légende pour chaque position"
    xticks(legende_axe_x, (libelle_spectacle[0], libelle_spectacle[1], libelle_spectacle[2], libelle_spectacle[3], libelle_spectacle[4]))
    
    "Réglage de la taille du graphique"
    plt = matplotlib.pyplot.gcf()
    plt.set_size_inches(15, 8)
    "Génération d'une image du graphique et affichage de cette image dans l'application"
    plt.savefig("img/classement_spectacles_recette.png")
    plt.clear()
    fenetre_application.inserer_image("img/classement_spectacles_recette.png")
    
    fenetre_application.inserer_separateur(1080, couleur="#000000")
    fenetre_application.inserer_list_boutons(liste_labels, liste_actions)

def recuperation_recettes_spectacles() :
    """
    Récupération des recettes par spectacle
    """
    #bloc_instructions
    connexion = connexion_base_de_donnees()
    stockage_de_requete = connexion.cursor()
    liste_recettes_par_spectacle = []
    
    requete = "SELECT libelle, SUM(reservation.nb_places*prix) AS recette FROM place, representation, reservation, spectacle WHERE reservation.id_place = place.id AND place.id_representation = representation.id AND representation.id_spectacle = spectacle.id GROUP BY libelle ORDER BY recette DESC"
    stockage_de_requete.execute(requete)
    
    for element in stockage_de_requete.fetchall() :
        liste_recettes_par_spectacle.append(element)
            
    connexion.close()
    
    return liste_recettes_par_spectacle
    
    
def lanceur_application() :
    """
    Déterminer une routine de lancement d'application
    """
    #bloc_instructions
    afficher_fenetre(construire_contenu_fenetre("accueil"))

"""
Exécution du programme
"""
fenetre_application = Agro_App("Méliès Manager")

"Offset de la requête SQL"
offset_consulter_spectacles = 0
offset_consulter_places = 0
offset_consulter_clients = 0
lanceur_application()