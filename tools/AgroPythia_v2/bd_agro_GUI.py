# -*- coding: utf-8 -*-
"""
Created on Fri Mar 11 11:43:44 2016

@author: chmartin
"""
from __future__ import unicode_literals
import sqlite3
import tkinter as tk
from tkinter import font as tkFont
from tkinter import ttk as ttk



class foreign_key :
    def __init__(self, _champ_source, _table_cible, _champ_cible):
        self.champ_source = _champ_source
        self.table_cible = _table_cible
        self.champ_cible = _champ_cible
    def __str__(self):
        #return self.champ_source.nom+' --> '+self.table_cible.nom+'.'+self.champ_cible.nom        
        return self.champ_source.nom+' --> '+self.table_cible+'.'+self.champ_cible        
        
class champ :
    def __init__(self, _nom, _type, _null, _pk):
        self.nom = _nom
        self.type = _type
        self.pk = _pk
        self.fkey = None
        self.null = _null        
        
    def __str__(self):
        return self.nom + ' : ' + self.type + '\t' + str(self.null) + '\t' + str(self.pk)      

    def set_fk(self,table_name, champ_name):
        self.fkey=[table_name, champ_name]
        
    def GUI(self, frame, i, colors, num_color):
        #self.frame = tk.LabelFrame(frame)
        label_nom = tk.Label(frame, text=self.nom,font = "Courier 9", anchor=tk.W, justify=tk.LEFT, background=colors[num_color])
        label_nom.grid(row=i, column=0, sticky=tk.E+tk.W)
        label_type = tk.Label(frame, text=self.type,font = "Courier 9", anchor=tk.W, justify=tk.LEFT, background=colors[num_color])
        label_type.grid(row=i, column=1, sticky=tk.E+tk.W)
        if self.null :
            label_null = tk.Label(frame, text='NN',font = "Courier 9", anchor=tk.W, justify=tk.LEFT, background=colors[num_color])
        else :
            label_null = tk.Label(frame, text='',font = "Courier 9", anchor=tk.W, justify=tk.LEFT, background=colors[num_color])
        label_null.grid(row=i, column=2, sticky=tk.E+tk.W)
        #♥num_color = (num_color+1)%2
        if self.fkey :
            label_fk = tk.Label(frame, text='--> '+self.fkey[0]+'.'+self.fkey[1],font = "Courier 9", anchor=tk.W, justify=tk.LEFT, background=colors[num_color])
            label_fk.grid(row=i, column=3, sticky=tk.E+tk.W)
        num_color = (num_color+1)%2
        if self.pk :
            f = tkFont.Font(label_nom, label_nom.cget("font"))
            f.configure(underline = True)
            label_nom.configure(font=f)
     
        

class table : 
    def __init__(self, cnx, _nom) :
        self.nom = _nom#.upper()
        self.champs = []
        self.fkeys = []
        self.load_structure(cnx)
        
    def add_champ(self, _champ):
        self.champs.append(_champ)
        
    def add_fkey(self, _fkey):
        self.fkeys.append(_fkey)
        
    def find_champ(self, name):
        result = None
        for champ in self.champs :
            if champ.nom == name :
                result = champ
        return result
        
    def __str__(self):
        chaine = self.nom + '\n'
        for champ in self.champs :
            chaine += champ.__str__() + '\n'
        for fkey in self.fkeys :
            chaine += fkey.__str__() + '\n'
        return chaine
    
    def comp_GUI (self, frame, colors) :
        i=0
        self.comp_frame=tk.LabelFrame(frame, bg='LemonChiffon3')
        label_nom = tk.Label(self.comp_frame, text=self.nom,font = "Courier 10 bold", background=colors[i])
        label_nom.pack(fill=tk.X)
        i+=1
        champ_frame = tk.LabelFrame(self.comp_frame, bg='LemonChiffon3')
        champ_frame.pack(fill=tk.X)
        for champ in self.champs :
            champ.GUI(champ_frame, i, colors, i%2)
            i+=1
            
    def OnHsb(self,*args):
        self.list_results.xview(*args)
        self.list_champs.xview(*args)            
            
    def cont_GUI(self,baseDonn,frame,colors) :
        requete = u'select * from %s' % self.nom
        sav = baseDonn.row_factory  # sauvegarde de l'ancienne valeur (None par défaut)
        baseDonn.row_factory = sqlite3.Row
        cur = baseDonn.cursor()
        baseDonn.row_factory = sav # retour à l'ancienne valeur
        cur.execute(requete)	   # exécution de la requête SQL 
        
        self.cont_frame = tk.LabelFrame(self.frame, text='', bd=0)
        
        r = cur.fetchone()
        j=1
        if r : #Si on a des données correspondant à la requête
            tk.Label(frame, text="Voici le contenu de la table "+self.nom+" :",font = "Verdana 10").pack(anchor=tk.W, pady='8')# padx='8',
        
            #On crée la liste qui permettra de les afficher 
            self.list_results = tk.Listbox(self.cont_frame, font='courier 10', height=8)
            self.list_champs = tk.Listbox(self.cont_frame, font='courier 10 bold', height=1)
            scroll_v = tk.Scrollbar(self.cont_frame, orient=tk.VERTICAL, command=self.list_results.yview)  
            scroll_h = tk.Scrollbar(self.cont_frame, orient=tk.HORIZONTAL, command=self.OnHsb)         
            self.list_results['xscrollcommand'] = scroll_h.set
            self.list_results['yscrollcommand'] = scroll_v.set        
            
            #on récupère les noms des colonnes
            champs = r.keys()
        
            donnees = []
            for value in r :
                donnees.append([value])
            #onstocke les données pour la mise en page ultérieure   
            for enreg in cur: 	
                for i in range(len(enreg)) :
                    donnees[i].append(enreg[i])    
            #on cherche par champs la chaine la plus longue
            lenghts = []
            i=0
            for liste in donnees :
                temp = []
                for value in liste :
                    #val = value.encode('utf-8')
                    #print val
                    #valeur = len(value)
                    #print valeur
                    #temp.append(len(unicode(value)))
                    temp.append(len(str(value)))
                temp.append(len(champs[i]))
                i+=1
                lenghts.append(max(temp))
            
            #affichage des intitulés des colonnes        
            ligne = ''
            for i in range(len(champs)) :
                #ligne += unicode(str(champs[i]))+(lenghts[i]+5)*' ' 
                #ligne += unicode(str(champs[i]))+(lenghts[i]-len(champs[i])+5)*' '                     
                ligne += str(champs[i])+(lenghts[i]-len(champs[i])+5)*' '
            #ligne = ligne.encode('utf-8')           
            self.list_champs.pack(fill=tk.BOTH, expand=True)
            self.list_champs.insert('end',ligne)
    
            scroll_h.pack(side=tk.BOTTOM, fill=tk.X, expand=True)
            self.list_results.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
            scroll_v.pack(fill=tk.Y, expand=True)
    
            #affichage des lignes de résultats    
            for i in range(len(donnees[0])) :
                ligne = ''
                for j in range(len(donnees)) :
                    #ligne += unicode(unicode(donnees[j][i]))+(lenghts[j]+5)*' '
                    #ligne += unicode(unicode(donnees[j][i]))+(lenghts[j]-len(unicode(donnees[j][i]))+5)*' '
                    #print ('+', (lenghts[j]-len(str(donnees[j][i]))+5)*' ', '+')                    
                    ligne += str(donnees[j][i])+(lenghts[j]-len(str(donnees[j][i]))+5)*' '
    
                self.list_results.insert('end', ligne)
                
                if ((i-1)%2) :
                    self.list_results.itemconfigure(i, background='cornsilk2') 
    
            self.list_champs.config(state=tk.DISABLED)
            self.list_results.config(activestyle='none')
            #print self.list_results.itemconfig(1)
            
        else :
            tk.Label(self.cont_frame, text="\nIl n'y a pas de données dans cette table !",font = "Verdana 11").pack(anchor=tk.W, fill=tk.BOTH, expand=True)
            

            
    def GUI (self, baseDonn, root_frame, colors):
        #root_frame.update()
        self.frame = tk.Frame(root_frame)
        
        self.comp_GUI(self.frame, colors)
        self.comp_frame.pack(side=tk.LEFT, padx=10, pady=10)
                
        self.cont_GUI(baseDonn, self.frame, colors)
        self.cont_frame.pack(fill=tk.BOTH, expand=True)
            
    def load_structure(self,cnx):
        """
        Ajoute à la table ses champs et clés
        """
        cur = cnx.cursor()
        cur.execute("PRAGMA table_info(%s)" % self.nom)
        for enreg in cur :
            #print enreg
            new_champ=champ(enreg[1],enreg[2],enreg[3],enreg[5])
            self.add_champ(new_champ)
        cur.execute("PRAGMA foreign_key_list(%s)" % self.nom)
        for enreg in cur :
            #print enreg
            champ_source = self.find_champ(enreg[3])
            champ_source.set_fk(enreg[2], enreg[4])
            if champ_source :
                new_fkeys=foreign_key(champ_source,enreg[2],enreg[4])
            else :
                print ("pb le champ source n'a pas été trouvé")
            self.add_fkey(new_fkeys)
        cur.close()
        
    def load_data(self,baseDonn):
        """
        Ajoute à la table ses données
        """
        requete = 'select * from ' + self.nom
        sav = baseDonn.row_factory  # sauvegarde de l'ancienne valeur (None par défaut)
        baseDonn.row_factory = sqlite3.Row
        cur = baseDonn.cursor()
        baseDonn.row_factory = sav # retour à l'ancienne valeur
        cur.execute(requete)	   # exécution de la requête SQL 
        
        donnees = []        
        r = cur.fetchone()

        if r : #Si on a des données correspondant à la requête
                   

            for value in r :
                donnees.append([value])
            #onstocke les données pour la mise en page ultérieure   
            for enreg in cur: 	
                for i in range(len(enreg)) :
                    donnees[i].append(enreg[i])    
            #on cherche par champs la chaine la plus longue
            lenghts = []
            for liste in donnees :
                temp = []
                for value in liste :
                    temp.append(len(str(value)))
                lenghts.append(max(temp))
            

           
        


        
class schema_bd : 
    def __init__(self, cnx, _nom):
        self.nom = _nom
        self.tables = []
        self.load(cnx, _nom)
        
    def add_table(self, _table):
        self.tables.append(_table)
        
    def __str__(self):
        chaine = self.nom + '\n\n'
        for table in self.tables :
            chaine += table.__str__() + '\n'
        return chaine
        
    def GUI(self, baseDonn, root_frame, colors):
        #print self
        if self.tables!= [] :
            self.box = ttk.Notebook(root_frame, width=800, height=228)
            for table in self.tables :
                table.GUI(baseDonn, root_frame, colors)
                self.box.add(table.frame, text=table.nom)    
        else :
            self.box = tk.Frame(root_frame, width=800, height=258)
            tk.Label(self.box, text='La base de données est vide !').pack()

    def load (self, cnx, basename):
        cur = cnx.cursor()
        cur.execute("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name")
        for enreg in cur: 	# Affichage du résultat
            new_table = table(cnx, enreg[0])    
            self.add_table(new_table)
        cur.close()



    

#baseDonn = sqlite3.connect('client_commande.sqlite')
#schema = schema_bd(baseDonn,'client_commande')
##baseDonn = sqlite3.connect('db_pdb.sqlite')
##schema = schema_bd(baseDonn,'db_pdb')
#
#print schema
#
#fenetre = tk.Tk()
#fenetre.title("Explorateur de bases de données SQLite")
#
#fenetre['bg']='LemonChiffon3' # couleur de fond 
#fenetre.geometry("1200x650+5+5")
#
#schema.GUI(baseDonn, fenetre,['white','LemonChiffon3'])
#schema.box.pack(fill=tk.X)
#
#fenetre.mainloop()
#        